const ALIGNMENTS = new Set(["LG", "NG", "CG", "LN", "N", "CN", "LE", "NE", "CE"] as const);

const ALIGNMENT_TRAITS = new Set(["chaotic", "evil", "good", "lawful"] as const);

export { ALIGNMENTS, ALIGNMENT_TRAITS };
